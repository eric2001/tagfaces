# TagFaces
![TagFaces](./screenshot.jpg) 

## Description
TagFaces is a module for Gallery 3 which will allow gallery users to assign an existing tag to a region of a photo. That region will then have it's own mouse-over caption (which will be the name of the tag) and will also be a click-able link (which will allow visitors to view other photos with the same tag). TagFaces requires that the Tag module be installed and active in order to function properly.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "tagfaces" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu.  While on the modules menu, you may also install Tags if you haven't already. You will have a "Options->Tag faces" menu for any photos that you have view and edit privileges on.

## History
**Version 2.2.1:**
> - Fixed weird RTL bug.
> - Released 01 April 2010.
>
> Download: [Version 2.2.1](/uploads/75e2b52efa60efd3f813e0bd9201c371/tagfaces221.zip)

**Version 2.2.0:**
> - Updated for recent API changes in current Gallery 3 Git.
> - Fixed issue were selecting a region of a photo did not populate the x1y1x2y2 boxes.
> - Fixed issue with tag links not working.
> - Fixed access::verify_csrf() issue.
> - Fixed issue were boxes don't show up in the right spot.
> - Fixed bug that caused the entire faces table to get deleted whenever a user tried to delete one or more tagged faces.
> - Fixed a similar bug that caused the entire faces table to get deleted whenever a face-tagged photo is deleted.
> - Added code for deleting notes from existing photos.
> - Tested everything against Gallery 3 Git (as of commit 38f2784fbbb0661dc57627d2878cb640bbffe271).
> - Released 12 January 2010.
>
> Download: [Version 2.2.0](/uploads/fe5498bd4cb538e7bbb1eb002282622c/tagfaces220.zip)

**Version 2.1.0:**
> - Merged in ckieffer's CSS changes for Gallery 3 Git
> - Made a few minor changes for compatibility with git.
> - Tested everything against git (as of commit b6c1ba7ea6416630b2a44b3df8400a2d48460b0a)
> - Released 14 October 2009
>
> Download: [Version 2.1.0](/uploads/32bc483b1e5095d0914dc3d11ee20469/tagfaces210.zip)

**Version 2.0.0:**
> - Added a description field for tags.
> - Added the ability to assign "notes" (title/description) to a region of the photo without tagging it.
> - Automatic deletion of face/note data when the photo is deleted
> - Released 27 August 2009
>
> Download: [Version 2.0.0](/uploads/0163949b92b32f9ce043690bf1051cfd/tagfaces200.zip)

**Version 1.1.0:**
> - The module now has the ability to draw a box over the tagged region of the photo when the mouse moves over it.
> - Released 24 August 2009
>
> Download: [Version 1.1.0](/uploads/005cb74f43ef093c3d6e3051fcefe770/tagfaces110.zip)

**Version 1.0.1:**
> - Bugfix for recent git changes to Gallery core.
> - Released on 16 August 2009
>
> Download: [Version 1.0.1](/uploads/c5af73a31c4d423e2e87da2a026bb4f0/tagfaces101.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 14 August 2009
>
> Download: [Version 1.0.0](/uploads/4a43d66c069335686be414c32437dcbc/tagfaces100.zip)
